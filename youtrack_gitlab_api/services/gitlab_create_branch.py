from youtrack_gitlab_api.utils.helpers import get_branches_by_name
from youtrack_gitlab_api.constants import Status
import logging


def create_branch_if_not_exists(project, new_branches, ref_branch):
    existed_branches = get_branches_by_name(project, new_branches[0])

    if len(existed_branches) > 0 and set(new_branches) == set(existed_branches):
        return {'status': Status.CREATED, 'message': 'Ветки уже существуют!'}

    created_branches = []
    not_created = []

    for new_branch in new_branches:
        if new_branch not in existed_branches:
            try:
                br = project.branches.create({'branch': new_branch, 'ref': ref_branch})

                created_branches.append(br.name)
            except Exception as e:
                not_created.append(new_branch)
                logging.error(e)

    if set(created_branches) == set(new_branches):
        return {'status': Status.SUCCESS, 'message': f'Были созданы следующие ветки {", ".join(created_branches)}!'}

    if len(not_created) > 0:
        return {'status': Status.ERROR, 'message': f'Не были созданы следующие ветки {", ".join(not_created)}!'}

    return {'status': Status.ERROR, 'message': 'Произошла ошибка на сервере ветки не были созданы!'}
