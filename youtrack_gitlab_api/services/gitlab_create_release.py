import logging

from youtrack_gitlab_api.constants import Status
from youtrack_gitlab_api.services.gitlab_create_branch import create_branch_if_not_exists
from youtrack_gitlab_api.services.gitlab_create_mr import create_merge_request_if_not_exists


def create_release_merge_request(project, kwargs):
    try:
        release_branch_info = {
            'tasks': kwargs['tasks'],
            'source_branch': kwargs['source_branch'],
            'target_branch': kwargs['target_branch'],
            'title': kwargs['title'],
            'description': kwargs['description']
        }

        result = create_branch_if_not_exists(project, [f'{release_branch_info["source_branch"]}'],
                                             release_branch_info['target_branch'])
        if result['status'] == Status.CREATED or result['status'] == Status.SUCCESS:
            release_mr = create_merge_request_if_not_exists(project, release_branch_info)

            if release_mr['status'] == Status.SUCCESS:
                release_mr_url = release_mr['mr'].web_url

                return {"status": Status.SUCCESS, 'message': f"MR для {release_branch_info['source_branch']} создан",
                        'url': release_mr_url}
            elif release_mr['status'] == Status.ERROR:
                return {"status": Status.ERROR,
                        'message': release_mr['message']}

        return {"status": Status.ERROR, 'message': f'Не удалось создать релизную ветку {kwargs["source_branch"]}'}
    except Exception as e:
        logging.error(e)
        return {"status": Status.ERROR, 'message': f'Не удалось создать релизную ветку {kwargs["source_branch"]}'}
