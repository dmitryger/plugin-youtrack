from youtrack_gitlab_api.utils.helpers import get_branches_by_name
from youtrack_gitlab_api.constants import Status
import logging


def create_branch_work(project, new_branch, ref_branch):
    existed_branches = get_branches_by_name(project, new_branch)

    if len(existed_branches) > 0:
        return {'status': Status.SUCCESS, 'message': f'Ветка {new_branch} уже существует!'}

    created_branch = ''
    not_created = ''

    if new_branch not in existed_branches:
        try:
            br = project.branches.create({'branch': new_branch, 'ref': ref_branch})
            created_branch = br.name
        except Exception as e:
            not_created = new_branch
            logging.error(e)

    if created_branch != '':
        return {'status': Status.SUCCESS, 'message': f'Была создана следующая ветка {created_branch}!'}

    if not_created != '':
        return {'status': Status.ERROR, 'message': f'Не удалось создать следующую ветку {not_created}!'}

    return {'status': Status.ERROR, 'message': 'Произошла ошибка на сервере ветки не были созданы!'}
