import gitlab

from youtrack_gitlab_api.environ import PROJECT_TOKEN, GITLAB_URL


class Gitlab:
    def __init__(self):
        self.gl = gitlab.Gitlab(url=GITLAB_URL, private_token=PROJECT_TOKEN)

    def get_gl_connection(self):
        return self.gl