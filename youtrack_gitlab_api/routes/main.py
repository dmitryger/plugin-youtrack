from flask import Blueprint, request, jsonify
from youtrack_gitlab_api.services.gitlab_connection import Gitlab
from youtrack_gitlab_api.environ import PROJECT_ID
from youtrack_gitlab_api.services.gitlab_create_branch_work import create_branch_work
from youtrack_gitlab_api.services.gitlab_create_branch import create_branch_if_not_exists
from youtrack_gitlab_api.services.gitlab_create_mr import create_merge_request_if_not_exists
from youtrack_gitlab_api.services.gitlab_develop_mr import create_develop_mr
from youtrack_gitlab_api.services.gitlab_create_release import create_release_merge_request
from youtrack_gitlab_api.services.gitlab_delete_branch import delete_branch_if_exists
from youtrack_gitlab_api.services.gitlab_delete_mr import delete_merge_request_if_exists
from youtrack_gitlab_api.services.gitlab_update_tasks_release import update_tasks_release
from youtrack_gitlab_api.services.smtp_service import send_email
from youtrack_gitlab_api.constants import Status
import logging

routes = Blueprint('routes', __name__)


@routes.route('/', methods=['GET'])
def main():
    return 'Start app!'


@routes.route('/delete-release', methods=['POST'])
def delete_release_mr():
    try:
        data = request.json

        gitlab = Gitlab()
        connection = gitlab.get_gl_connection()

        project = connection.projects.get(PROJECT_ID)

        data_response = {
            'title': f'Релиз {data["version"]}',
            'source_branch': f'{data["source_branch"]}/{data["version"]}',
            'target_branch': data['target_branch'],
        }

        result_delete_mr = delete_merge_request_if_exists(project, data_response)
        result_delete_branch = delete_branch_if_exists(project, data_response)

        if result_delete_mr['status'] == Status.SUCCESS and result_delete_branch['status'] == Status.SUCCESS:
            return jsonify({'message': 'Release успешно удален', 'status': 200}), 200
        elif result_delete_mr['status'] == Status.ERROR or result_delete_branch['status'] == Status.ERROR:
            return jsonify({'message': 'Release не удален', 'status': 500}), 500

    except Exception as e:
        logging.error(e)
        return jsonify({'message': 'Произошла ошибка при удалении релиза!', 'status': 500}), 500


@routes.route('/update-release-tasks', methods=['POST'])
def update_release_tasks():
    try:
        data = request.json

        gitlab = Gitlab()
        connection = gitlab.get_gl_connection()

        project = connection.projects.get(PROJECT_ID)

        data_response = {
            'target_branch': f'{data["target_branch"]}/{data["version"]}',
            'tasks': data['tasks']
        }

        update_tasks = update_tasks_release(project, data_response)

        if update_tasks['status'] == Status.SUCCESS:
            return jsonify({'messages': update_tasks['messages'], 'status': 201}), 201
        elif update_tasks['status'] == Status.ERROR:
            return jsonify({'messages': update_tasks['messages'], 'status': 500}), 500
    except Exception as e:
        logging.error(e)
        return jsonify({'message': 'Задачи не обновлены в релизе!', 'status': 500}), 500


@routes.route('/create-release-mr', methods=['POST'])
def create_release_mr():
    try:
        data = request.json

        gitlab = Gitlab()
        connection = gitlab.get_gl_connection()

        project = connection.projects.get(PROJECT_ID)

        data_response = {
            'title': f'Релиз {data["version"]}',
            'description': f'{", ".join(task["url"] for task in data["tasks"])}',
            'source_branch': f'{data["source_branch"]}/{data["version"]}',
            'target_branch': data['target_branch'],
            'master_branch': data['master_branch'],
            'tasks': data['tasks'],
            'recipients_email': data['recipients_email']
        }

        release_mr = create_release_merge_request(project, data_response)

        if release_mr['status'] == Status.SUCCESS:
            send_email(data_response['recipients_email'], data_response['title'],
                       f'Релиз успешно создан - {release_mr["url"]}. :Задачи в релизе: {data_response["description"]}')

            return jsonify({'message': release_mr['message'], 'url': release_mr['url'], 'status': 201}), 201
        elif release_mr['status'] == Status.ERROR:
            send_email(data_response['recipients_email'], data_response['title'],
                       f'Произошла ошибка при создании релиза.')
            return jsonify({'message': release_mr['message'], 'status': 500}), 500
    except Exception as e:
        logging.error(e)
        return jsonify({'message': 'Произошла ошибка при создании релиза!', 'status': 500}), 500


@routes.route('/create-develop-mr', methods=['POST'])
def create_develop_merge_request():
    try:
        data = request.json

        gitlab = Gitlab()
        connection = gitlab.get_gl_connection()

        project = connection.projects.get(PROJECT_ID)

        data_response = {
            'title': data['title'],
            'description': data['url'],
            'source_branch': data['source_branch'],
            'target_branch': data['target_branch'],
            'master_branch': data['master_branch'],
            'recipients_email': data['recipients_email']
        }

        result = create_branch_if_not_exists(project, [data_response['target_branch']], data_response['master_branch'])

        if result['status'] == Status.CREATED or result['status'] == Status.SUCCESS:
            data = create_develop_mr(project, data_response)

            if data['status'] == Status.SUCCESS:
                send_email(data_response['recipients_email'], data_response['title'],
                           f'MR {data["mr"].web_url} смерджен в {data_response["target_branch"]}')
                return jsonify({'message': f'MR {data["mr"].web_url} смерджен в {data_response["target_branch"]}',
                                'status': 201}), 201
            elif data['status'] == Status.ERROR:
                return jsonify({'message': data['message'], 'status': 500}), 500
        elif result['status'] == Status.ERROR:
            return jsonify({'message': result['message'], 'status': 500}), 500

    except Exception as error:
        logging.error(error)
        return jsonify({'message': 'Произошла ошибка при мердже mr!', 'status': 500}), 500


@routes.route('/create-mr', methods=['POST'])
def create_merge_request():
    try:
        data = request.json

        gitlab = Gitlab()
        connection = gitlab.get_gl_connection()

        project = connection.projects.get(PROJECT_ID)

        data_response = {
            'title': f"{data['source_branch']}: {data['title']}",
            'description': data['url'],
            'source_branch': data['source_branch'],
            'target_branch': data['target_branch'],
            'develop_branch': data['develop_branch'],
            'recipients_email': data['recipients_email']
        }

        result = create_branch_if_not_exists(project, [data_response['target_branch']], data_response['develop_branch'])

        if result['status'] == Status.CREATED or result['status'] == Status.SUCCESS:
            data = create_merge_request_if_not_exists(project, data_response)
            if data['status'] == Status.CREATED:
                send_email(data_response['recipients_email'], data_response['title'], f'MR для {data_response["target_branch"]} обновлен. Ссылка {data["mr"].web_url}')
                return jsonify(
                    {'message': f'MR для {data_response["target_branch"]} обновлен', 'webUrl': data['mr'].web_url,
                     'status': 200}), 200
            elif data['status'] == Status.SUCCESS:
                send_email(data_response['recipients_email'], data_response['title'],
                           f'MR для {data_response["target_branch"]} создан. Ссылка {data["mr"].web_url}')
                return jsonify(
                    {'message': f'MR для {data_response["target_branch"]} создан', 'webUrl': data['mr'].web_url,
                     'status': 201}), 201
            elif data['status'] == Status.ERROR:
                send_email(data_response['recipients_email'], data_response['title'],
                           f'Не удалось создать mr.')
                return jsonify({'message': data['message'], 'status': 500}), 500
        elif result['status'] == Status.ERROR:
            return jsonify({'message': result['message'], 'status': 500}), 500
    except Exception as error:
        logging.error(error)
        return jsonify({'message': 'Произошла ошибка при создании mr!', 'status': 500}), 500


@routes.route('/create-branch-in-work', methods=['POST'])
def create_branch_in_work():
    try:
        data = request.json

        gitlab = Gitlab()
        connection = gitlab.get_gl_connection()

        project = connection.projects.get(PROJECT_ID)

        data_response = {
            'target_branch': data['target_branch'],
            'develop_branch': data['develop_branch'],
        }

        result = create_branch_work(project, data_response['target_branch'], data_response['develop_branch'])

        if result['status'] == Status.SUCCESS:
            return jsonify(
                {'message': result['message'],
                 'status': 201}), 201
        return jsonify(
            {'message': result['message'],
             'status': 500}), 500

    except Exception as error:
        logging.error(error)
        return jsonify({'message': 'Произошла ошибка при создании ветки!', 'status': 500}), 500


@routes.route('/test-connection', methods=['GET'])
def test_connection():
    try:
        gitlab = Gitlab()
        connection = gitlab.get_gl_connection()

        project = connection.projects.get(PROJECT_ID)
        if project:
            return jsonify({'message': f'Плагин подключен, Ваш проект: {project.name}', 'status': 200}), 200
        return jsonify({'message': 'Проект не найден, проверьте PROJECT_ID', 'status': 404}), 404
    except Exception as error:
        logging.error(error)
        return jsonify({'message': 'Ошибка подключения. Проверьте верно ли указан PROJECT_TOKEN или GITLAB_URL',
                        'status': 403}), 403


@routes.route('/test-send-email', methods=['POST'])
def test_send_email():
    try:
        data = request.json

        data_response = {
            'recipients_email': data['recipients_email']
        }

        send_email(data_response['recipients_email'], 'test', 'test')
        return jsonify({'message': f'Письмо отправлено', 'status': 200}), 200
    except Exception as error:
        logging.error(error)
        return jsonify({'message': 'Ошибка при отправке письма',
                        'status': 500}), 500
