# Документация к плагину

Плагин разделен на две части.
1)  youtrack-workflow - набор скриптов для youtrack рабочих процессов, разработанный для соединения с api gitlab
2)  youtrack-gitlab-api - gitlab api написанное на flask позволяющее работать с gitlab репозиторием

[workflow](https://gitlab.com/helpme-group/plugin-youtrack/-/blob/master/docs/workflow-api.md)
[gitlab-api](https://gitlab.com/helpme-group/plugin-youtrack/-/blob/master/docs/gitlab-api.md)
[smtp-api](https://gitlab.com/helpme-group/plugin-youtrack/-/blob/master/docs/smtp-api.md)