# Gitlab api

## Поднятие api плагина

Версия api плагина публикуется в [Docker hub](https://hub.docker.com/r/qwer342/youtrack-gitlab-api).
Подтянуть образ можно следующим образом:
```docker
docker pull qwer342/youtrack-gitlab-api
```

Для корректного поднятия требуется указать следующие переменные
```text
  app:
    image: qwer342/youtrack-gitlab-api:1.0.0
    ports:
      - "5000:5000"
    environment:
      - GITLAB_URL=<key> - ссылка на gitlab(https://gitlab.com)
      - PROJECT_TOKEN=<key> - токен для авторизации пользователя
      - PROJECT_ID=<key> - id проекта Settings -> General -> Projects Id
      - PORT=<key> - порт на котором будет поднято апи
      - DEBUG=False - включить дебаг режим
```