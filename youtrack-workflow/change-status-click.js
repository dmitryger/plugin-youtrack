const entities = require('@jetbrains/youtrack-scripting-api/entities');
const http = require('@jetbrains/youtrack-scripting-api/http');
const workflow = require('@jetbrains/youtrack-scripting-api/workflow');
const config = require('./config');

function makeBasicConnection() {
  return new http.Connection(config.CONFIG.baseUrl);
}

exports.rule = entities.Issue.action({
  title: 'test-connection-gitlab',
  command: 'test-connection-gitlab',
  guard: () => {
    return true;
  },
  action: (ctx) => {
    const connection = makeBasicConnection();
    connection.addHeader('Content-Type', 'application/json');
    const result = connection.getSync('test-connection');
    
    if (result.code === 200) {
       workflow.message(JSON.parse(result.response).message);
    } else if (result.code === 404) {
       workflow.check(result.code === 404,JSON.parse(result.response).message);
    } else if (result.code === 403) {
        workflow.check(result.code === 403, JSON.parse(result.response).message);
    }
  },
});