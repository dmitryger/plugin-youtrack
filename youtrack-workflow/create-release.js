const entities = require('@jetbrains/youtrack-scripting-api/entities');
const http = require('@jetbrains/youtrack-scripting-api/http');
const config = require('./config');
const request = require('./requests');

function makeBasicConnection() {
  return new http.Connection(config.CONFIG.baseUrl);
}

exports.rule = entities.Issue.action({
  title: 'Create-release',
  command: 'create-release',
  guard: (ctx) => {
    return ctx.issue.hasTag(config.CONFIG.releaseTag);
  },
  action: (ctx) => {
    const connection = makeBasicConnection();
    connection.addHeader('Content-Type', 'application/json');
    
    request.requestCreateReleaseMr(ctx, connection);
  }
});