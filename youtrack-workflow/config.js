const ACTIONS = {
  createMr: "create-mr",
  createDevelopMr: 'create-develop-mr',
  createReleaseMr: 'create-release-mr',
  deleteReleaseMr: 'delete-release',
  updateReleaseIssue: 'update-release-tasks',
  createBranchInWork: 'create-branch-in-work'
};

const STATE = {
  review: 'Ревью',
  release: 'В очереди на релиз',
  inWork: 'Разработка'
};

const BRANCH_FIELDS = {
  developBranch: 'Develop ветка',
  stageBranch: 'Stage ветка',
  masterBranch: 'Master ветка'
};

const RELEASE_BRANCH_NAME = 'release';

const RECIPIENTS_EMAIL = [];

const CONFIG = {
  baseUrl: 'https://youtrack-dmitryitis.amvera.io/',
  releaseTag: 'epic',
  versionField: 'Версия релиза',
  states: [{
    state: STATE.review,
    stageBranch: BRANCH_FIELDS.stageBranch,
    developBranch: BRANCH_FIELDS.developBranch,
    action: ACTIONS.createMr,
    excludeTags: ['epic']
  },
   {
    state: STATE.release,
    action: ACTIONS.createDevelopMr,
    masterBranch: BRANCH_FIELDS.masterBranch,
    developBranch: BRANCH_FIELDS.developBranch,
    excludeTags: ['epic']
   },
   {
   	state: STATE.inWork,
     action: ACTIONS.createBranchInWork,
     developBranch: BRANCH_FIELDS.developBranch,
     excludeTags: ['epic']
   }
  ],
};

exports.ACTIONS = ACTIONS;
exports.BRANCH_FIELDS = BRANCH_FIELDS;
exports.RELEASE_BRANCH_NAME = RELEASE_BRANCH_NAME;
exports.CONFIG = CONFIG;
exports.STATE = STATE;
exports.RECIPIENTS_EMAIL  = RECIPIENTS_EMAIL;