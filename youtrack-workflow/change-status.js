const entities = require('@jetbrains/youtrack-scripting-api/entities');
const http = require('@jetbrains/youtrack-scripting-api/http');
const config = require('./config');
const request = require('./requests');
const helpers = require('./helpers');

function makeBasicConnection() {
  return new http.Connection(config.CONFIG.baseUrl);
}

function requests(stateItem, ctx, connection) {
  if (stateItem.action === config.ACTIONS.createMr) request.requestCreateStageMr(stateItem, ctx, connection);
  else if (stateItem.action === config.ACTIONS.createDevelopMr) request.requestCreateDevelopMr(stateItem, ctx, connection);
  else if (stateItem.action === config.ACTIONS.createBranchInWork) request.requestCreateBranchInWork(stateItem, ctx, connection);
}

exports.rule = entities.Issue.onChange({
  title: 'change-state-issue',
  guard: () => {
    return true;
  },
  action: (ctx) => {
    const connection = makeBasicConnection();
    connection.addHeader('Content-Type', 'application/json');
    
    config.CONFIG.states.forEach(item => {
      const isExludeIssue = helpers.isExcludeIssue(ctx.issue, item.excludeTags);
      if (item.state === ctx.issue.fields.State.name && !isExludeIssue) {
        requests(item, ctx, connection);
      }
    });
  },
});