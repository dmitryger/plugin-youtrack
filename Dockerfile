FROM python:3.11.9-alpine3.19

ENV PYTHONUNBUFFERED 1

COPY requirements.txt ./requirements.txt

RUN pip install -r requirements.txt

COPY app.py /app/app.py
COPY youtrack_gitlab_api /app/youtrack_gitlab_api

# Устанавливаем переменные окружения во время сборки образа
ARG GITLAB_URL
ARG PROJECT_TOKEN
ARG PROJECT_ID
ARG PORT
ARG DEBUG
ENV GITLAB_URL=$GITLAB_URL
ENV PROJECT_TOKEN=$PROJECT_TOKEN
ENV PROJECT_ID=$PROJECT_ID
ENV PORT=$PORT
ENV DEBUG=$DEBUG
ENV SMTP_SERVER=$SMTP_SERVER
ENV SMTP_PASSWORD=$SMTP_PASSWORD
ENV SMTP_HOST=$SMTP_HOST
ENV SMTP_PORT=$SMTP_PORT

# Создаем файл .env
RUN echo "GITLAB_URL=$GITLAB_URL" >> .env
RUN echo "PROJECT_TOKEN=$PROJECT_TOKEN" >> .env
RUN echo "PROJECT_ID=$PROJECT_ID" >> .env
RUN echo "PORT=$PORT" >> .env
RUN echo "DEBUG=$DEBUG" >> .env
RUN echo "SMTP_SERVER=$SMTP_SERVER" >> .env
RUN echo "SMTP_PASSWORD=$SMTP_PASSWORD" >> .env
RUN echo "SMTP_HOST=$SMTP_HOST" >> .env
RUN echo "SMTP_PORT=$SMTP_PORT" >> .env


WORKDIR /app

CMD ["gunicorn", "--bind", "0.0.0.0:5000", "app:app"]